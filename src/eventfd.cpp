#include <stream9/linux/eventfd/eventfd.hpp>

#include <stream9/linux/namespace.hpp>

#include <string>

#include <stream9/bits.hpp>
#include <stream9/json.hpp>
#include <stream9/linux/read.hpp>
#include <stream9/linux/write.hpp>

namespace stream9::linux {

static std::string
flags_to_symbol(int flags)
{
    return bits::ored_flags_to_symbol(flags, {
        STREAM9_FLAG_ENTRY(EFD_CLOEXEC),
        STREAM9_FLAG_ENTRY(EFD_NONBLOCK),
        STREAM9_FLAG_ENTRY(EFD_SEMAPHORE),
    });
}

static int
make_eventfd(unsigned int initval, int flags)
{
    auto rc = ::eventfd(initval, flags | EFD_CLOEXEC);
    if (rc == -1) {
        throw error {
            "eventfd()",
            linux::make_error_code(errno),
        };
    }

    return rc;
}

/*
 * class eventfd
 */
eventfd::
eventfd(unsigned int initval/*= 0*/, int flags/*= 0*/)
    try : fd { make_eventfd(initval, flags) }
{}
catch (...) {
    rethrow_error({
        { "initval", initval },
        { "flags", flags_to_symbol(flags) },
    });
}

outcome<std::uint64_t, lx::errc> eventfd::
read() noexcept
{
    std::uint64_t val;

    auto o_n = lx::read(*this, val);
    if (o_n) {
        return val;
    }
    else {
        return { st9::error_tag(), o_n.error() };
    }
}

outcome<void, lx::errc> eventfd::
write(std::uint64_t value) noexcept
{
    auto o_n = lx::write(*this, value);
    if (o_n) {
        assert(*o_n == sizeof(value));
        return {};
    }
    else {
        return o_n.error();
    }
}

} // namespace stream9::linux
