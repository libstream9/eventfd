#ifndef STREAM9_LINUX_EVENTFD_EVENTFD_HPP
#define STREAM9_LINUX_EVENTFD_EVENTFD_HPP

#include <cstdint>

#include <stream9/linux/error.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/outcome.hpp>

#include <sys/eventfd.h>

namespace stream9::linux {

class eventfd : public fd
{
public:
    // essential
    explicit eventfd(unsigned int initval = 0, int flags = 0);

    eventfd(eventfd const&) = delete;
    eventfd& operator=(eventfd const&) = delete;
    eventfd(eventfd&&) = default;
    eventfd& operator=(eventfd&&) = default;

    // command
    outcome<std::uint64_t, lx::errc>
    read() noexcept;

    outcome<void, lx::errc>
    write(std::uint64_t value) noexcept;
};

} // namespace stream9::linux

#endif // STREAM9_LINUX_EVENTFD_EVENTFD_HPP
